Author: Jens Kaminsky

IMPORTANT: It's a maven project!

features:
 + basic features

 + usability features
   + enable/disable buttons
   + highlight invalid fields
   + multilingual (default DE + EN), easy to extend
   + basic css styling
   + search function (first- + lastName) with levenshtein algorithm

 + subjective wow "cough"
   + undo/redo functionality (implemenented with cmd pattern)
   + usage of state machine for edit/add view -> reusability of ui elements
   + usage of wrapper pattern for MemberManager, easy extensibility
   + "clean and maintainable" code
   + self made icon

 - died dreams:
   - matchmaking with the usage of elo