package ch.fhnw.languages;

import ch.fhnw.model.Gender;

public class MMViewLangEnglish implements MMViewLang {

    @Override
    public String toString() {
        return "English";
    }

    @Override
    public String stageTitle() {
        return "Member Manager";
    }

    @Override
    public String menuFile() {
        return "File";
    }

    @Override
    public String menuLanguage() {
        return "Language";
    }

    @Override
    public String menuUndo() {
        return "Undo";
    }

    @Override
    public String menuRedo() {
        return "Redo";
    }

    @Override
    public String firstName() {
        return "First Name";
    }

    @Override
    public String lastName() {
        return "Last Name";
    }

    @Override
    public String gender() {
        return "Gender";
    }

    @Override
    public String phone() {
        return "Phone";
    }

    @Override
    public String email() {
        return "Mail";
    }

    @Override
    public String zipCode() {
        return "Zip Code";
    }

    @Override
    public String city() {
        return "City";
    }

    @Override
    public String street() {
        return "Street";
    }

    @Override
    public String membershipType() {
        return "Membership Type";
    }

    @Override
    public String expirationDate() {
        return "Membership Expiration Date";
    }

    @Override
    public String changeAvatar() {
        return "Change Avatar";
    }

    @Override
    public String saveChanges() {
        return "Save Changes";
    }

    @Override
    public String create() {
        return "Create";
    }

    @Override
    public String cancel() {
        return "Cancel";
    }

    @Override
    public String genderName(Gender gender) {
        switch (gender) {
            case Male:
                return "Male";
            case Female:
                return "Female";
            default:
                throw new EnumConstantNotPresentException(Gender.class, gender.toString());
        }
    }

    @Override
    public Gender genderEnum(String gender) {
        switch (gender) {
            case "Male":
                return Gender.Male;
            case "Female":
                return Gender.Female;
            default:
                throw new EnumConstantNotPresentException(Gender.class, gender);
        }
    }

    @Override
    public String eloScore() {
        return "Elo Score";
    }

    @Override
    public String topThree() {
        return "Top Three";
    }


}
