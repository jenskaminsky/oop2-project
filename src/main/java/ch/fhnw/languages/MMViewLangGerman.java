package ch.fhnw.languages;

import ch.fhnw.model.Gender;

public class MMViewLangGerman implements MMViewLang {

    @Override
    public String toString() {
        return "Deutsch";
    }

    @Override
    public String stageTitle() {
        return "Mitglieder Verwaltung";
    }

    @Override
    public String menuFile() {
        return "Datei";
    }

    @Override
    public String menuLanguage() {
        return "Sprache";
    }

    @Override
    public String menuUndo() {
        return "Rückgängig";
    }

    @Override
    public String menuRedo() {
        return "Wiederholen";
    }

    @Override
    public String firstName() {
        return "Vorname";
    }

    @Override
    public String lastName() {
        return "Nachname";
    }

    @Override
    public String gender() {
        return "Geschlecht";
    }

    @Override
    public String phone() {
        return "Telefon";
    }

    @Override
    public String email() {
        return "e-mail";
    }

    @Override
    public String zipCode() {
        return "Postleitzahl";
    }

    @Override
    public String city() {
        return "Stadt";
    }

    @Override
    public String street() {
        return "Strasse";
    }

    @Override
    public String membershipType() {
        return "Mitgliedschaftstyp";
    }

    @Override
    public String expirationDate() {
        return "Ablaufdatum M.";
    }

    @Override
    public String changeAvatar() {
        return "Anzeigebild ändern";
    }

    @Override
    public String saveChanges() {
        return "Änderungen speichern";
    }

    @Override
    public String create() {
        return "Erstellen";
    }

    @Override
    public String cancel() {
        return "Abbrechen";
    }

    @Override
    public String genderName(Gender gender) {
        switch (gender) {
            case Male:
                return "Mann";
            case Female:
                return "Frau";
            default:
                throw new EnumConstantNotPresentException(Gender.class, gender.toString());
        }
    }

    @Override
    public Gender genderEnum(String gender) {
        switch (gender) {
            case "Mann":
                return Gender.Male;
            case "Frau":
                return Gender.Female;
            default:
                throw new EnumConstantNotPresentException(Gender.class, gender);
        }
    }

    @Override
    public String eloScore() {
        return "Elo Wert";
    }

    @Override
    public String topThree() {
        return "Top drei";
    }
}
