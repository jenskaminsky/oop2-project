package ch.fhnw.languages;

import ch.fhnw.model.Gender;

public interface MMViewLang {
    
    public String toString();

    public String stageTitle();

    public String menuFile();

    public String menuLanguage();

    public String menuUndo();

    public String menuRedo();

    public String firstName();

    public String lastName();

    public String gender();

    public String phone();

    public String email();

    public String zipCode();

    public String city();

    public String street();

    public String membershipType();

    public String expirationDate();

    public String changeAvatar();

    public String saveChanges();

    public String create();
    
    public String cancel();

    public String genderName(Gender gender);

    public Gender genderEnum(String gender);

    public String eloScore();

    public String topThree();
}
