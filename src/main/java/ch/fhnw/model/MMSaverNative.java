package ch.fhnw.model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.nio.file.Path;

public class MMSaverNative implements MMSaver {

    private Path filePath;

    public MMSaverNative(Path filePath) {
        this.filePath = filePath;
    }

    @Override
    public void save(MemberList memberList) {
        try (FileOutputStream fileOut = new FileOutputStream(filePath.toString());
                ObjectOutputStream out = new ObjectOutputStream(fileOut)) {
            out.writeObject(memberList);
        } catch (IOException ex) {
            System.out.println("ERROR SAVING");
        }
    }

    @Override
    public MemberList load() {
        MemberList loaded = new MemberList(new Member[0]);
        try {
            try (FileInputStream fileIn = new FileInputStream(filePath.toString()); ObjectInputStream in = new ObjectInputStream(fileIn)) {
                loaded = (MemberList) in.readObject();
            }
        } catch (FileNotFoundException ex) {
            System.out.println("ERROR LOADING");
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println("ERROR LOADING");
        }
        return loaded;
    }
}
