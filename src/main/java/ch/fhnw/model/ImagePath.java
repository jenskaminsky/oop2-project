package ch.fhnw.model;

import java.io.Serializable;

public class ImagePath implements Serializable {

    public final String FOLDER = "avatars";
    public final String name;
    public final String fileExtension;

    public ImagePath(String filePath) {
        int idxFolder = filePath.lastIndexOf('/');
        int idxExtension = filePath.lastIndexOf('.');

        name = filePath.substring(idxFolder + 1, idxExtension);
        fileExtension = filePath.substring(idxExtension + 1);
    }

    public String getPath() {
        return FOLDER + '/' + name + '.' + fileExtension;
    }

    @Override
    public String toString() {
        return "ImagePath{" + "FOLDER=" + FOLDER + ", name=" + name + ", fileExtension=" + fileExtension + '}';
    }
}
