package ch.fhnw.model;

import java.util.Random;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class MemberManagerBasic implements MemberManager {

    private MMSaver mMSaver;

    private final Random random = new Random();
    private ObservableList<Member> members = FXCollections.observableArrayList();

    public MemberManagerBasic(MMSaver mMSaver) {
        this.mMSaver = mMSaver;
    }

    @Override
    public void create(Member member) {
        members.add(member);
    }

    @Override
    public ObservableList<Member> read() {
        return members;
    }

    @Override
    public void update(Member before, Member after) {
        members.remove(before);
        members.add(after);
    }

    @Override
    public void delete(Member member) {
        members.remove(member);
    }

    @Override
    public void saveToFile() {
        Member[] arrayM = new Member[members.size()];
        members.toArray(arrayM);
        mMSaver.save(new MemberList(arrayM));
    }

    @Override
    public void loadFromFile() {
        members.clear();
        members.addAll(mMSaver.load().MEMBERS);
        
    }
}
