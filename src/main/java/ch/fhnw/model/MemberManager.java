package ch.fhnw.model;

import javafx.collections.ObservableList;

public interface MemberManager {

    public void create(Member member);
    public ObservableList<Member> read();
    public void update(Member before, Member after);
    public void delete(Member member);

    public void saveToFile();
    public void loadFromFile();

}
