package ch.fhnw.model;

import java.io.Serializable;

public enum Gender implements Serializable {
    Male,
    Female;
}
