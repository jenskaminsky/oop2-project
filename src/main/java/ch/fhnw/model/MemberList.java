package ch.fhnw.model;

import java.io.Serializable;

/**
 * this class only exists because of stupid GSON
 */
public class MemberList implements Serializable {

    public final Member[] MEMBERS;

    public MemberList(Member[] members) {
        this.MEMBERS = members;
    }
}
