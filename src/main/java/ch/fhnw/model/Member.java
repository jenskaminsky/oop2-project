package ch.fhnw.model;

import java.io.Serializable;

public class Member implements Serializable {

    public final ImagePath avatarPath;
    
    public final String firstName;
    public final String lastName;

    public final Gender gender;

    public final String phone;
    public final String email;

    public final short eloScore;
    
    public final Address address;
    public final Membership membership;

    public Member(ImagePath avatarPath, String firstName, String lastName, Gender gender,
            String phone, String email, short eloScore, Address address, Membership membership) {
        this.avatarPath = avatarPath;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.phone = phone;
        this.email = email;
        this.eloScore = eloScore;
        this.address = address;
        this.membership = membership;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
    
    public String getEloScore(){
        return String.valueOf(eloScore);
    }
}
