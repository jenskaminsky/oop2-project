package ch.fhnw.model;

import java.util.Enumeration;
import java.util.Stack;
import javafx.collections.ObservableList;

public class MemberManagerHistory implements MemberManager {

    private final MemberManager memberManager;

    private Stack<MMCommand> commandsUndo = new Stack();
    private Stack<MMCommand> commandsRedo = new Stack();

    public MemberManagerHistory(MemberManager memberManager) {
        this.memberManager = memberManager;
    }

    @Override
    public void loadFromFile() {
        memberManager.loadFromFile();
    }

    @Override
    public void saveToFile() {
        memberManager.saveToFile();
    }

    @Override
    public void create(Member member) {
        MMCommandCreate cmd = new MMCommandCreate(member);
        addAndExecuteCommand(cmd);

        printHistories();
    }

    @Override
    public void update(Member before, Member after) {
        MMCommandUpdate cmd = new MMCommandUpdate(before, after);
        addAndExecuteCommand(cmd);

        printHistories();
    }

    @Override
    public void delete(Member member) {
        MMCommandDelete cmd = new MMCommandDelete(member);
        addAndExecuteCommand(cmd);

        printHistories();
    }

    @Override
    public ObservableList<Member> read() {
        return memberManager.read();
    }

    public void redo() {
        if (canRedo()) {
            MMCommand todo = commandsRedo.pop();
            todo.execute();
            commandsUndo.push(todo);
        } else {
            System.out.println("can't redo");
        }
        printHistories();
    }

    public void undo() {

        if (canUndo()) {
            MMCommand todo = commandsUndo.pop();
            todo.undo();
            commandsRedo.push(todo);
        } else {
            System.out.println("can't undo");
        }
        printHistories();
    }

    public boolean canUndo() {
        return !commandsUndo.empty();
    }

    public boolean canRedo() {
        return !commandsRedo.empty();
    }

    private void addAndExecuteCommand(MMCommand cmd) {
        commandsRedo.removeAllElements();

        boolean executed = cmd.execute();
        if (executed) {
            commandsUndo.push(cmd);
        } else {
            System.out.println("error");
        }
    }

    public interface MMCommand {

        public boolean execute();

        public void undo();

        public String text();
    }

    private class MMCommandCreate implements MMCommand {

        private boolean wasExecuted = false;
        private final Member member;

        public MMCommandCreate(Member member) {
            this.member = member;
        }

        @Override
        public boolean execute() {
            if (!wasExecuted) {
                memberManager.create(member);
                wasExecuted = true;
            }
            return wasExecuted;
        }

        @Override
        public void undo() {
            if (wasExecuted) {
                memberManager.delete(member);
                wasExecuted = false;
            }
        }

        @Override
        public String text() {
            return "CREATE " + member.firstName + member.lastName;
        }
    }

    private class MMCommandUpdate implements MMCommand {

        private boolean wasExecuted = false;

        private final Member before;
        private final Member after;

        public MMCommandUpdate(Member before, Member after) {
            this.before = before;
            this.after = after;
        }

        @Override
        public boolean execute() {
            if (!wasExecuted) {
                memberManager.update(before, after);
                wasExecuted = true;
            }
            return wasExecuted;
        }

        @Override
        public void undo() {
            if (wasExecuted) {
                memberManager.update(after, before);
                wasExecuted = false;
            }
        }

        @Override
        public String text() {
            return "UPDATE " + before.firstName + before.lastName;
        }
    }

    private class MMCommandDelete implements MMCommand {

        private boolean wasExecuted = false;
        private final Member member;

        public MMCommandDelete(Member member) {
            this.member = member;
        }

        @Override
        public boolean execute() {
            if (!wasExecuted) {
                memberManager.delete(member);
                wasExecuted = true;
            }
            return wasExecuted;
        }

        @Override
        public void undo() {
            if (wasExecuted) {
                memberManager.create(member);
                wasExecuted = false;
            }
        }

        @Override
        public String text() {
            return "DELETE " + member.firstName + member.lastName;
        }
    }

    public Enumeration<MMCommand> commandsUndo() {
        return commandsUndo.elements();
    }

    public Enumeration<MMCommand> commandsRedo() {
        return commandsRedo.elements();
    }

    public void printHistories() {
//        System.out.println("------------- undo -------------");
//        for (MMCommand mMCommand : commandsUndo) {
//            System.out.println(mMCommand.text());
//        }
//        System.out.println("------------- redo -------------");
//        for (MMCommand mMCommand : commandsRedo) {
//            System.out.println(mMCommand.text());
//        }
//        System.out.println("");
//        System.out.println("");
    }
}
