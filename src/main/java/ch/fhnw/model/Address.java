package ch.fhnw.model;

import java.io.Serializable;

public class Address implements Serializable {

    public short zipCode;
    public String city;
    public String Street;

    public Address(short zip, String city, String Street) {
        this.zipCode = zip;
        this.city = city;
        this.Street = Street;
    }
}
