package ch.fhnw.model;

import java.io.Serializable;
import java.util.Date;

public class Membership implements Serializable {

    private final String type;
    private final Date expirationDate;

    public Membership(String type, Date expirationDate) {
        this.type = type;
        this.expirationDate = expirationDate;
    }

    public String type() {
        return type;
    }

    public Date expirationDate() {
        return expirationDate;
    }
}
