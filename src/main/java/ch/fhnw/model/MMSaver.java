package ch.fhnw.model;

public interface MMSaver {

    public void save(MemberList memberList);
    public MemberList load();
}
