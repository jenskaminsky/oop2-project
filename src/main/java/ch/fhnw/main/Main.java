package ch.fhnw.main;

import java.util.Date;
import ch.fhnw.model.Address;
import ch.fhnw.model.Gender;
import ch.fhnw.model.MMSaver;
import ch.fhnw.model.MMSaverNative;
import ch.fhnw.model.MemberManagerBasic;
import ch.fhnw.model.MemberManagerHistory;
import ch.fhnw.model.Membership;
import ch.fhnw.view.MemberManagerView;
import java.nio.file.Paths;
import javafx.application.Application;
import javafx.stage.Stage;
import ch.fhnw.languages.MMViewLang;
import ch.fhnw.languages.MMViewLangEnglish;
import ch.fhnw.languages.MMViewLangGerman;
import ch.fhnw.model.ImagePath;
import ch.fhnw.model.Member;
import ch.fhnw.view.AddModeController;
import ch.fhnw.view.RemoveController;
import ch.fhnw.view.RedoController;
import ch.fhnw.view.SaveController;
import ch.fhnw.view.SearchController;
import ch.fhnw.view.ShowTopTreeController;
import ch.fhnw.view.UndoController;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class Main extends Application {

    private MemberManagerView view;
    private MemberManagerHistory model;

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        String SAVE_FILE_NAME = "members.ser";
        
        MMSaver mMSaver = new MMSaverNative(Paths.get(SAVE_FILE_NAME));
        MemberManagerBasic mmb = new MemberManagerBasic(mMSaver);
        model = new MemberManagerHistory(mmb);
        model.loadFromFile();

        MMViewLang english = new MMViewLangEnglish();
        MMViewLang german = new MMViewLangGerman();

        List<MMViewLang> languages = new ArrayList();
        languages.add(german);
        languages.add(english);

        view = new MemberManagerView(primaryStage, languages, english, model);
        view.initStage();

        EventHandler<ActionEvent> redoController = new RedoController(model, view);
        EventHandler<ActionEvent> undoController = new UndoController(model, view);
        EventHandler<ActionEvent> removeController = new RemoveController(model, view);
        EventHandler<ActionEvent> addController = new AddModeController(model, view);
        EventHandler<ActionEvent> saveController = new SaveController(model, view);
        EventHandler<ActionEvent> starController = new ShowTopTreeController(model, view);
        ChangeListener searchController = new SearchController(model, view);

        view.addRedoBtnController(redoController);
        view.addUndoBtnController(undoController);
        view.addRemoveBtnController(removeController);
        view.addAddBtnController(addController);
        view.addSaveBtnController(saveController);
        view.addStarBtnController(starController);
        view.addSearchFieldTFController(searchController);

        view.start();
    }
}
