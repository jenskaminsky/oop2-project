package ch.fhnw.view;

import static java.util.stream.Collectors.toCollection;
import ch.fhnw.model.Member;
import org.apache.commons.text.similarity.LevenshteinDistance;
import ch.fhnw.model.MemberManager;
import ch.fhnw.model.MemberManagerHistory;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class SearchController implements ChangeListener {

    MemberManager model;
    MemberManagerView view;
    LevenshteinDistance ld = LevenshteinDistance.getDefaultInstance();

    public SearchController(MemberManagerHistory model, MemberManagerView view) {
        this.model = model;
        this.view = view;
    }

    /**
     * algorith to collect into ObservableList
     * https://stackoverflow.com/questions/33849538/collectors-lambda-return-observable-list
     */
    @Override
    public void changed(ObservableValue observable, Object oldValue, Object newValue) {
        if (newValue != null) {
            String searchValue = String.valueOf(newValue);
            if (searchValue.length() > 0) {
                ObservableList<Member> members = model.read().stream()
                        .filter((m -> ld.apply(searchValue, m.firstName + " " + m.lastName) < 15))
                        .sorted((o1, o2) -> {
                            return ld.apply(o1.firstName + " " + o1.lastName, searchValue) - ld.apply(o2.firstName + " " + o2.lastName, searchValue);
                        })
                        .collect(toCollection(FXCollections::observableArrayList));
                view.memberTable.setItems(members);
            } else {
                view.memberTable.setItems(model.read());
            }
        }
    }
}
