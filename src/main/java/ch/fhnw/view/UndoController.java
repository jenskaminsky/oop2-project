package ch.fhnw.view;

import ch.fhnw.model.MemberManagerHistory;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class UndoController implements EventHandler<ActionEvent> {

    MemberManagerHistory model;
    MemberManagerView view;

    public UndoController(MemberManagerHistory model, MemberManagerView view) {
        this.model = model;
        this.view = view;
    }

    @Override
    public void handle(ActionEvent event) {
        model.undo();
        view.updateUndoRedoBtn();

    }
}
