package ch.fhnw.view;

import ch.fhnw.model.MemberManagerHistory;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class RedoController implements EventHandler<ActionEvent> {

    MemberManagerHistory model;
    MemberManagerView view;

    public RedoController(MemberManagerHistory model, MemberManagerView view) {
        this.model = model;
        this.view = view;
    }


    @Override
    public void handle(ActionEvent event) {
        model.redo();
        view.updateUndoRedoBtn();
    }
}
