package ch.fhnw.view;

import ch.fhnw.model.Member;
import ch.fhnw.model.MemberManager;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class RemoveController implements EventHandler<ActionEvent> {

    MemberManager model;
    MemberManagerView view;

    public RemoveController(MemberManager model, MemberManagerView view) {
        this.model = model;
        this.view = view;
    }

    @Override
    public void handle(ActionEvent event) {
        Member selectedMember = (Member) view.memberTable.getSelectionModel().getSelectedItem();
        model.delete(selectedMember);
        view.updateUndoRedoBtn();
    }
}
