package ch.fhnw.view;

import ch.fhnw.languages.MMViewLang;

public interface ChangeableLanguage {

    public void changeLanguage(MMViewLang mMViewLang);
}
