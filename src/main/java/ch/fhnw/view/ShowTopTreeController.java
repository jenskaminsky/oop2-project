package ch.fhnw.view;

import ch.fhnw.model.MemberManager;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ShowTopTreeController implements EventHandler<ActionEvent> {

    MemberManager model;
    MemberManagerView view;

    public ShowTopTreeController(MemberManager model, MemberManagerView view) {
        this.model = model;
        this.view = view;
    }

    @Override
    public void handle(ActionEvent event) {
        TopTreeView ttv = new TopTreeView(model, view.currentLang.topThree());
        ttv.start();
    }
}
