package ch.fhnw.view;

import javafx.stage.Stage;
import ch.fhnw.languages.MMViewLang;
import ch.fhnw.model.Member;
import ch.fhnw.model.MemberManagerHistory;
import java.util.List;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public final class MemberManagerView implements ChangeableLanguage, ChangeableDetailView {

    private final static String CSS_PATH = "file:src/main/resources/MemberManagerView.css";
    private final static String UNDO_ICON_PATH = "file:src/main/resources/undo.png";
    private final static String REDO_ICON_PATH = "file:src/main/resources/redo.png";
    private final static String ADD_ICON_PATH = "file:src/main/resources/add.png";
    private final static String REMOVE_ICON_PATH = "file:src/main/resources/remove.png";
    private final static String SAVE_ICON_PATH = "file:src/main/resources/save.png";
    private final static String STAR_ICON_PATH = "file:src/main/resources/star.png";
    private final static String STAGE_ICON_PATH = "file:src/main/resources/icon.png";

    protected List<MMViewLang> availableLang;
    protected MMViewLang currentLang;
    private MemberManagerHistory model;

    protected Stage stage;

    private BorderPane mainPane;

    protected TableView memberTable;

    protected ChoiceBox<MMViewLang> langCB;
    protected Button saveBtn = new Button();
    protected Button starBtn = new Button();

    protected Button undoBtn = new Button();
    protected Button redoBtn = new Button();

    protected Button addBtn = new Button();
    protected Button removeBtn = new Button();

    protected TextField searchField = new TextField();

    protected DetailView detailView;

    public MemberManagerView(Stage stage, List<MMViewLang> availableLang, MMViewLang defaultLang, MemberManagerHistory memberManager) {
        this.stage = stage;
        this.model = memberManager;
        this.availableLang = availableLang;
        this.currentLang = defaultLang;
    }

    public void initStage() {
        stage.setTitle(currentLang.stageTitle());
        stage.setResizable(false);
        stage.getIcons().add(new Image(STAGE_ICON_PATH));

        mainPane = new BorderPane();

        ToolBar menuBar = initMenuBar();
        Pane listView = initListView();
        initDetailView();

        mainPane.setTop(menuBar);
        mainPane.setLeft(listView);
        mainPane.setCenter(detailView);

        Scene scene = new Scene(mainPane);
        scene.getStylesheets().add(CSS_PATH);
        stage.setScene(scene);
    }

    public void start() {
        stage.show();
    }

    public void stop() {
        stage.hide();
    }

    private ToolBar initMenuBar() {
        ToolBar menuBar = new ToolBar();

        langCB = new ChoiceBox(FXCollections.observableArrayList(availableLang));
        langCB.getSelectionModel().select(currentLang);
        langCB.getSelectionModel().selectedIndexProperty().addListener(new LanguageListener());

        Image saveImg = new Image(SAVE_ICON_PATH);
        saveBtn.setGraphic(new ImageView(saveImg));

        Image undoImg = new Image(UNDO_ICON_PATH);
        undoBtn.setGraphic(new ImageView(undoImg));

        Image redoImg = new Image(REDO_ICON_PATH);
        redoBtn.setGraphic(new ImageView(redoImg));

        Image starImg = new Image(STAR_ICON_PATH);
        starBtn.setGraphic(new ImageView(starImg));

        menuBar.getItems().addAll(langCB, starBtn, saveBtn, undoBtn, redoBtn);
        return menuBar;
    }

    private void initDetailView() {
        detailView = new DetailViewAdd(model, currentLang, this);
    }

    private Pane initListMenu() {
        HBox cmdBar = new HBox();

        Image addImg = new Image(ADD_ICON_PATH);
        addBtn.setGraphic(new ImageView(addImg));

        Image removeImg = new Image(REMOVE_ICON_PATH);
        removeBtn.setGraphic(new ImageView(removeImg));
        removeBtn.setDisable(true);

        searchField.setText("");
        searchField.setPromptText("search ...");
        HBox.setHgrow(searchField, Priority.ALWAYS);

        cmdBar.getChildren().addAll(addBtn, removeBtn, searchField);
        return cmdBar;
    }

    private void initMemberTable() {
        memberTable = new TableView();
        memberTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        TableColumn firstName = new TableColumn(currentLang.firstName());
        firstName.setCellValueFactory(new PropertyValueFactory<Member, String>("firstName"));
        TableColumn lastName = new TableColumn(currentLang.lastName());
        lastName.setCellValueFactory(new PropertyValueFactory<Member, String>("lastName"));
        TableColumn eloScore = new TableColumn(currentLang.eloScore());
        eloScore.setCellValueFactory(new PropertyValueFactory<Member, String>("eloScore"));

        memberTable.getColumns().addAll(firstName, lastName, eloScore);
        memberTable.setItems(model.read());
        memberTable.getSelectionModel().selectedItemProperty().addListener(this::onTableRowSelect);
    }

    private Pane initListView() {
        VBox listView = new VBox();
        listView.setPrefWidth(300);

        Pane listMenu = initListMenu();
        initMemberTable();
        VBox.setVgrow(memberTable, Priority.ALWAYS);

        listView.getChildren().addAll(listMenu, memberTable);
        return listView;
    }

    private void onTableRowSelect(Observable o, Object before, Object after) {
        if (after != null) {
            Member m = (Member) after;
            detailView.setEditMode(m);

            removeBtn.setDisable(false);
        } else {
            removeBtn.setDisable(true);
        }
    }

    public void addUndoBtnController(EventHandler<ActionEvent> eventHandler) {
        undoBtn.setOnAction(eventHandler);
    }

    public void addRedoBtnController(EventHandler<ActionEvent> eventHandler) {
        redoBtn.setOnAction(eventHandler);
    }

    public void addRemoveBtnController(EventHandler<ActionEvent> eventHandler) {
        removeBtn.setOnAction(eventHandler);
    }

    public void addAddBtnController(EventHandler<ActionEvent> eventHandler) {
        addBtn.setOnAction(eventHandler);
    }

    public void addSaveBtnController(EventHandler<ActionEvent> eventHandler) {
        saveBtn.setOnAction(eventHandler);
    }

    public void addStarBtnController(EventHandler<ActionEvent> eventHandler) {
        starBtn.setOnAction(eventHandler);
    }

    public void addSearchFieldTFController(ChangeListener<String> listener) {
        searchField.textProperty().addListener(listener);
    }

    public void updateUndoRedoBtn() {
        undoBtn.setDisable(!model.canUndo());
        redoBtn.setDisable(!model.canRedo());
    }

    @Override
    public void changeLanguage(MMViewLang mMViewLang) {
        stage.setTitle(currentLang.stageTitle());
        TableColumn firstName = new TableColumn(currentLang.firstName());
        firstName.setCellValueFactory(new PropertyValueFactory<Member, String>("firstName"));
        TableColumn lastName = new TableColumn(currentLang.lastName());
        lastName.setCellValueFactory(new PropertyValueFactory<Member, String>("lastName"));
        TableColumn eloScore = new TableColumn(currentLang.eloScore());
        eloScore.setCellValueFactory(new PropertyValueFactory<Member, String>("eloScore"));

        memberTable.getColumns().clear();
        memberTable.getColumns().addAll(firstName, lastName, eloScore);
    }

    @Override
    public void changeDetailview(DetailView detailView) {
        this.detailView = detailView;
        mainPane.setCenter(detailView);
    }

    private class LanguageListener implements ChangeListener {

        @Override
        public void changed(ObservableValue observable, Object oldValue, Object newValue) {
            if (newValue != null) {
                currentLang = availableLang.get((Integer) newValue);
                changeLanguage(currentLang);
                detailView.changeLanguage(currentLang);
            }
        }
    }
}
