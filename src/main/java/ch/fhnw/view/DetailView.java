package ch.fhnw.view;

import ch.fhnw.view.elements.TextFieldValidateEmpty;
import ch.fhnw.languages.MMViewLang;
import ch.fhnw.model.Address;
import ch.fhnw.model.Gender;
import ch.fhnw.model.ImagePath;
import ch.fhnw.model.Member;
import ch.fhnw.model.MemberManager;
import ch.fhnw.model.Membership;
import ch.fhnw.view.elements.ChoiceBoxValidateSelected;
import ch.fhnw.view.elements.DatePickerValidateSelected;
import ch.fhnw.view.elements.TextFieldValidateEmail;
import ch.fhnw.view.elements.TextFieldValidateShort;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javax.imageio.ImageIO;
import org.imgscalr.Scalr;
import org.apache.commons.validator.routines.EmailValidator;

public abstract class DetailView extends GridPane implements ChangeableLanguage {

    protected MemberManager model;

    protected MemberManagerView context;

    private final static String DEFAULT_AVATAR_PATH = "src/main/resources/avatar.png";
    protected MMViewLang mMViewLang;

    protected ImageView avatarImgView = new ImageView();

    private Button changeAvatarBtn;
    protected TextField avatarPathTF = new TextField();

    private TextFieldValidateEmpty firstNameTF = new TextFieldValidateEmpty();
    private TextFieldValidateEmpty lastNameTF = new TextFieldValidateEmpty();
    private ChoiceBoxValidateSelected genderCB = new ChoiceBoxValidateSelected();
    private TextFieldValidateEmpty phoneTF = new TextFieldValidateEmpty();
    private TextFieldValidateEmail emailTF = new TextFieldValidateEmail();
    private TextFieldValidateShort zipTF = new TextFieldValidateShort();
    private TextFieldValidateEmpty cityTF = new TextFieldValidateEmpty();
    private TextFieldValidateEmpty streetTF = new TextFieldValidateEmpty();
    private TextFieldValidateEmpty memTypeTF = new TextFieldValidateEmpty();
    private DatePickerValidateSelected expirationDateDP = new DatePickerValidateSelected();
    protected TextField eloScoreTF = new TextField();

    private Label firstNameLbl = new Label();
    private Label lastNameLbl = new Label();
    private Label genderLbl = new Label();
    private Label phoneLbl = new Label();
    private Label emailLbl = new Label();
    private Label zipLbl = new Label();
    private Label cityLbl = new Label();
    private Label streetLbl = new Label();
    private Label memTypeLbl = new Label();
    private Label expirationDateLbl = new Label();
    private Label eloScoreLbl = new Label();

    protected Button confirmBtn;
    private Button cancelBtn;

    public DetailView(MemberManager memberManager, MMViewLang mMViewLang, MemberManagerView context) {
        this.model = memberManager;
        this.mMViewLang = mMViewLang;
        this.context = context;

        setHgap(20);
        setVgap(10);
        setPadding(new Insets(10, 10, 10, 10));

        getColumnConstraints().add(0, new ColumnConstraints(200));
        getColumnConstraints().add(1, new ColumnConstraints(200));

        avatarImgView.setFitHeight(200);
        avatarImgView.setFitWidth(200);

        changeAvatarBtn = new Button(mMViewLang.changeAvatar());
        changeAvatarBtn.setOnAction(new ChangeAvatarController(this));

        avatarPathTF.setDisable(true);
        eloScoreTF.setDisable(true);

        ObservableList<String> genders = FXCollections.observableArrayList(Arrays.asList(Gender.values()).stream()
                .map((g -> mMViewLang.genderName(g)))
                .collect(Collectors.toList()));
        genderCB.setItems(genders);

        firstNameLbl.setText(mMViewLang.firstName());
        lastNameLbl.setText(mMViewLang.lastName());
        genderLbl.setText(mMViewLang.gender());
        phoneLbl.setText(mMViewLang.phone());
        emailLbl.setText(mMViewLang.email());
        zipLbl.setText(mMViewLang.zipCode());
        cityLbl.setText(mMViewLang.city());
        streetLbl.setText(mMViewLang.street());
        memTypeLbl.setText(mMViewLang.membershipType());
        expirationDateLbl.setText(mMViewLang.expirationDate());
        eloScoreLbl.setText(mMViewLang.eloScore());

        confirmBtn = new Button(mMViewLang.saveChanges());
        cancelBtn = new Button(mMViewLang.cancel());

        tryUpdateAvatar(DEFAULT_AVATAR_PATH, avatarImgView);

        add(avatarImgView, 0, 0);
        add(new VBox(changeAvatarBtn, avatarPathTF), 1, 0);
        add(firstNameLbl, 0, 2);
        add(firstNameTF, 1, 2);
        add(lastNameLbl, 0, 3);
        add(lastNameTF, 1, 3);
        add(genderLbl, 0, 4);
        add(genderCB, 1, 4);
        add(phoneLbl, 0, 5);
        add(phoneTF, 1, 5);
        add(emailLbl, 0, 6);
        add(emailTF, 1, 6);

        add(zipLbl, 0, 7);
        add(zipTF, 1, 7);
        add(cityLbl, 0, 8);
        add(cityTF, 1, 8);
        add(streetLbl, 0, 9);
        add(streetTF, 1, 9);

        add(memTypeLbl, 0, 10);
        add(memTypeTF, 1, 10);
        add(expirationDateLbl, 0, 11);
        add(expirationDateDP, 1, 11);

        add(eloScoreLbl, 0, 12);
        add(eloScoreTF, 1, 12);

        add(confirmBtn, 0, 14);
        add(cancelBtn, 1, 14);
    }

    protected static void tryUpdateAvatar(String avatarPath, ImageView avatarImgView) {
        InputStream pics;
        try {
            pics = new FileInputStream(avatarPath);
            BufferedImage before = ImageIO.read(pics);
            BufferedImage after = Scalr.resize(before, 200);
            Image image = SwingFXUtils.toFXImage(after, null);
            avatarImgView.setImage(image);
        } catch (IOException ex1) {
            System.out.println(avatarPath);
            System.out.println("CAUGHT");
            try {
                pics = new FileInputStream(DEFAULT_AVATAR_PATH);
                BufferedImage before = ImageIO.read(pics);
                BufferedImage after = Scalr.resize(before, 200);
                Image image = SwingFXUtils.toFXImage(after, null);
                avatarImgView.setImage(image);
            } catch (IOException ex2) {
            }
        }
    }

    public abstract void setEditMode(Member m);

    public abstract void setAddMode();

    protected void displayMember(Member member) {
        tryUpdateAvatar(member.avatarPath.getPath(), avatarImgView);

        avatarPathTF.setText(member.avatarPath.getPath());

        firstNameTF.setText(member.firstName);
        lastNameTF.setText(member.lastName);

        genderCB.getSelectionModel().select(mMViewLang.genderName(member.gender));

        phoneTF.setText(member.phone);
        emailTF.setText(member.email);

        zipTF.setText(String.valueOf(member.address.zipCode));
        cityTF.setText(member.address.city);
        streetTF.setText(member.address.Street);

        memTypeTF.setText(member.membership.type());
        LocalDate localDateExpirationDate = new java.sql.Date(member.membership.expirationDate().getTime()).toLocalDate();
        expirationDateDP.setValue(localDateExpirationDate);

        eloScoreTF.setText(String.valueOf(member.eloScore));
    }

    protected boolean areFieldsValid() {
        boolean firstNameValid = firstNameTF.getText().length() > 0;
        boolean lastNameValid = lastNameTF.getText().length() > 0;
        boolean genderValid = !genderCB.getSelectionModel().isEmpty();
        boolean phoneValid = phoneTF.getText().length() > 0;
        boolean emailValid = EmailValidator.getInstance().isValid(emailTF.getText());

        boolean eloScoreValid = true;
        try {
            Short.parseShort(eloScoreTF.getText());
        } catch (NumberFormatException ex) {
            eloScoreValid = false;
        }

        boolean zipValid = true;
        try {
            Short.parseShort(zipTF.getText());
        } catch (NumberFormatException ex) {
            zipValid = false;
        }
        boolean cityValid = cityTF.getText().length() > 0;
        boolean streetValid = streetTF.getText().length() > 0;

        boolean typeValid = memTypeTF.getText().length() > 0;

        boolean dateValid = true;
        try {
            java.sql.Date.valueOf(expirationDateDP.getValue());
        } catch (NullPointerException ex) {
            dateValid = false;
        }
        return firstNameValid && lastNameValid && genderValid && phoneValid && emailValid && zipValid && cityValid && streetValid && typeValid && dateValid && eloScoreValid;
    }

    @Override
    public void changeLanguage(MMViewLang mMViewLang) {
        firstNameLbl.setText(mMViewLang.firstName());
        lastNameLbl.setText(mMViewLang.lastName());
        genderLbl.setText(mMViewLang.gender());
        phoneLbl.setText(mMViewLang.phone());
        emailLbl.setText(mMViewLang.email());
        zipLbl.setText(mMViewLang.zipCode());
        cityLbl.setText(mMViewLang.city());
        streetLbl.setText(mMViewLang.street());
        memTypeLbl.setText(mMViewLang.membershipType());
        expirationDateLbl.setText(mMViewLang.expirationDate());
        eloScoreLbl.setText(mMViewLang.eloScore());

        confirmBtn.setText(mMViewLang.saveChanges());
        cancelBtn.setText(mMViewLang.cancel());
        Gender gender = null;
        Object selectedItem = genderCB.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            gender = this.mMViewLang.genderEnum((String) selectedItem);
        }

        ObservableList<String> genders = FXCollections.observableArrayList(Arrays.asList(Gender.values()).stream()
                .map((g -> mMViewLang.genderName(g)))
                .collect(Collectors.toList()));
        genderCB.setItems(genders);

        if (gender != null) {
            genderCB.getSelectionModel().select(mMViewLang.genderName(gender));
        }
        
        changeAvatarBtn.setText(mMViewLang.changeAvatar());
        
        this.mMViewLang = mMViewLang;
    }

    protected void clearDisplay() {
        avatarPathTF.setText("");
        firstNameTF.setText("");
        lastNameTF.setText("");
        genderCB.getSelectionModel().clearSelection();
        phoneTF.setText("");
        emailTF.setText("");
        zipTF.setText("");
        cityTF.setText("");
        streetTF.setText("");
        memTypeTF.setText("");
        expirationDateDP.setValue(null);
        eloScoreTF.setText("");
    }

    protected Member getMemberFromFields() {
        short zip = Short.parseShort(zipTF.getText());
        String city = cityTF.getText();
        String street = streetTF.getText();

        Address address = new Address(zip, city, street);

        String type = memTypeTF.getText();
        Date expirationDate = java.sql.Date.valueOf(expirationDateDP.getValue());

        Membership membership = new Membership(type, expirationDate);

        ImagePath avatarPath = new ImagePath(avatarPathTF.getText());
        String firstName = firstNameTF.getText();
        String lastName = lastNameTF.getText();
        Gender gender = mMViewLang.genderEnum((String) genderCB.getSelectionModel().getSelectedItem());
        String phone = phoneTF.getText();
        String email = emailTF.getText();

        short eloScore = Short.valueOf(eloScoreTF.getText());

        return new Member(avatarPath, firstName, lastName, gender, phone, email, eloScore, address, membership);
    }

    void validateFields() {
        firstNameTF.validate();
        lastNameTF.validate();
        genderCB.validate();
        phoneTF.validate();
        emailTF.validate();
        zipTF.validate();
        cityTF.validate();
        streetTF.validate();
        memTypeTF.validate();
        expirationDateDP.validate();
    }
}
