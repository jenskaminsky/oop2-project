package ch.fhnw.view;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.FileChooser;

public class ChangeAvatarController implements EventHandler<ActionEvent> {

    private static final String ROOT_PATH = "avatars/";

    private DetailView view;

    public ChangeAvatarController(DetailView view) {
        this.view = view;
    }

    @Override
    public void handle(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter imageFilter = new FileChooser.ExtensionFilter("Image files", "*.jpg", "*.png");
        fileChooser.getExtensionFilters().add(imageFilter);
        fileChooser.setTitle("Select Avatar Image");

        File selectedFile = fileChooser.showOpenDialog(view.context.stage);

        if (selectedFile != null) {
            String name = generateFileName();
            String newFileName = newFileName(selectedFile, name);
            try {
                copyFileToNewDestination(selectedFile, newFileName);
                view.avatarPathTF.setText(newFileName);
                view.tryUpdateAvatar(newFileName, view.avatarImgView);
            } catch (IOException ex) {
                System.out.println("ERROR FILE COPY");
            }
        }
    }

    private String generateFileName() {
        Random random = new Random();

        String name = generateRandomTenSmallLetters(random);

        while (doesNameExist(name)) {
            generateRandomTenSmallLetters(random);
        }
        return name;
    }

    private String generateRandomTenSmallLetters(Random random) {
        String name = "";
        for (int i = 0; i < 10; i++) {
            name += (char)('a' + random.nextInt(25));
        }
        return name;
    }

    private boolean doesNameExist(String name) {
        File root = new File(ROOT_PATH);

        File[] files = root.listFiles();
        for (File file : files) {
            if (file.getName().contains(name)) {
                return true;
            }
        }
        return false;
    }

    private void copyFileToNewDestination(File origin, String newPath) throws IOException {
        Files.copy(origin.toPath(), Paths.get(newPath));
    }

    private String newFileName(File origin, String name) {
        return ROOT_PATH + name + getExtension(origin.getName());
    }

    private String getExtension(String path) {
        int idxLastDot = path.lastIndexOf('.');
        return path.substring(idxLastDot, path.length());
    }
}
