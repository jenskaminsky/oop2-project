package ch.fhnw.view;

import ch.fhnw.languages.MMViewLang;
import ch.fhnw.model.Member;
import ch.fhnw.model.MemberManager;

public final class DetailViewEdit extends DetailView {

    public DetailViewEdit(MemberManager memberManager, MMViewLang mMViewLang, MemberManagerView context, Member member) {
        super(memberManager, mMViewLang, context);
        confirmBtn.setText(mMViewLang.saveChanges());
        confirmBtn.setOnAction(new SaveMemberChangesController(model, this, member, context));
        displayMember(member);
    }

    @Override
    public void setEditMode(Member member) {
        System.out.println("edit -> editmode");
        confirmBtn.setOnAction(new SaveMemberChangesController(model, this, member, context));
        displayMember(member);
    }

    @Override
    public void setAddMode() {
        System.out.println("edit -> addmode");
        DetailView nextState = new DetailViewAdd(model, mMViewLang, context);
        context.changeDetailview(nextState);
    }

    @Override
    public void changeLanguage(MMViewLang mMViewLang) {
        confirmBtn.setText(mMViewLang.saveChanges());
        super.changeLanguage(mMViewLang);
    }
}
