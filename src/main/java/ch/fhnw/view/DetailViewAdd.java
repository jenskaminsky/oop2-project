package ch.fhnw.view;

import ch.fhnw.languages.MMViewLang;
import ch.fhnw.model.Member;
import ch.fhnw.model.MemberManager;

public final class DetailViewAdd extends DetailView {

    public DetailViewAdd(MemberManager memberManager, MMViewLang mMViewLang, MemberManagerView context) {
        super(memberManager, mMViewLang, context);
        confirmBtn.setText(mMViewLang.create());
        confirmBtn.setOnAction(new AddMemberController(model, this, context));
        clearDisplay();
        avatarPathTF.setText("detault.png");
        eloScoreTF.setText("1200");
    }

    @Override
    public void setEditMode(Member member) {
        System.out.println("add -> editmode");
        DetailView nextState = new DetailViewEdit(model, mMViewLang, context, member);
        context.changeDetailview(nextState);
    }

    @Override
    public void setAddMode() {
        //do nothing, already in this mode
    }

    @Override
    public void changeLanguage(MMViewLang mMViewLang) {
        super.changeLanguage(mMViewLang);
        confirmBtn.setText(mMViewLang.create());
    }
}
