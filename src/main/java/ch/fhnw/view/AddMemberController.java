package ch.fhnw.view;

import ch.fhnw.model.Member;
import ch.fhnw.model.MemberManager;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class AddMemberController implements EventHandler<ActionEvent> {

    MemberManager model;
    DetailView view;
    MemberManagerView context;

    public AddMemberController(MemberManager model, DetailView view, MemberManagerView context) {
        this.model = model;
        this.view = view;
        this.context = context;
    }

    @Override
    public void handle(ActionEvent event) {
        view.validateFields();
        if (view.areFieldsValid()) {
            Member member = view.getMemberFromFields();
            model.create(member);
            context.updateUndoRedoBtn();
        }
    }
}
