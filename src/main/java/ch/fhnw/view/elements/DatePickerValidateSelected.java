package ch.fhnw.view.elements;

import javafx.beans.Observable;
import javafx.scene.control.DatePicker;

public class DatePickerValidateSelected extends DatePicker {

    {
        valueProperty().addListener(this::action);
    }

    private void action(Observable o) {
        validate();
    }

    private boolean isValid() {
        return getValue() != null;
    }

    public void validate() {
        if (isValid()) {
            setStyle("-fx-background-color: white;");
        } else {
            setStyle("-fx-background-color: red;");
        }
    }

}
