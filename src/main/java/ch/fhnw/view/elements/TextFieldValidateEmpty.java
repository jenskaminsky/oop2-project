package ch.fhnw.view.elements;

import javafx.beans.Observable;
import javafx.scene.control.TextField;

public class TextFieldValidateEmpty extends TextField {

    {
        this.setStyle("-fx-background-color: white;");
    }

    public TextFieldValidateEmpty() {
        this.textProperty().addListener(this::action);
    }

    private void action(Observable o, String oldValue, String newValue) {
        validate();
    }

    private boolean isValid() {
        return !getText().isEmpty();
    }

    public void validate() {
        if (isValid()) {
            setStyle("-fx-background-color: white;");
        } else {
            setStyle("-fx-background-color: red;");
        }
    }
}
