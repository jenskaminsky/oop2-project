package ch.fhnw.view.elements;

import javafx.beans.Observable;
import javafx.scene.control.ChoiceBox;

public class ChoiceBoxValidateSelected extends ChoiceBox {

    {
        getSelectionModel().selectedIndexProperty().addListener(this::action);
    }

    private void action(Observable o) {
        validate();
    }

    private boolean isValid() {
        return !getSelectionModel().isEmpty();
    }

    public void validate() {
        if (isValid()) {
            setStyle("-fx-background-color: white;");
        } else {
            setStyle("-fx-background-color: red;");
        }
    }

}
