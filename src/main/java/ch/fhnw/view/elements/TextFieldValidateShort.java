package ch.fhnw.view.elements;

import javafx.beans.Observable;
import javafx.scene.control.TextField;

public class TextFieldValidateShort extends TextField {

    {
        setStyle("-fx-background-color: white;");
    }

    public TextFieldValidateShort() {
        textProperty().addListener(this::action);
    }

    private void action(Observable o, String oldValue, String newValue) {
        validate();
    }

    private boolean isValid() {
        try {
            Short.parseShort(getText());
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    public void validate() {
        if (isValid()) {
            setStyle("-fx-background-color: white;");
        } else {
            setStyle("-fx-background-color: red;");
        }
    }
}
