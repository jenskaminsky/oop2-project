package ch.fhnw.view.elements;

import javafx.beans.Observable;
import javafx.scene.control.TextField;
import org.apache.commons.validator.routines.EmailValidator;

public class TextFieldValidateEmail extends TextField {

    {
        this.setStyle("-fx-background-color: white;");
    }

    public TextFieldValidateEmail() {
        this.textProperty().addListener(this::action);
    }

    private void action(Observable o, String oldValue, String newValue) {
        validate();
    }

    public boolean isValid() {
        return EmailValidator.getInstance().isValid(getText());
    }

    public void validate() {
        if (isValid()) {
            setStyle("-fx-background-color: white;");
        } else {
            setStyle("-fx-background-color: red;");
        }
    }
}
