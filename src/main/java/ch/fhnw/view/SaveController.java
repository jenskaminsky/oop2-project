package ch.fhnw.view;

import ch.fhnw.model.MemberManager;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class SaveController implements EventHandler<ActionEvent> {

    MemberManager model;
    MemberManagerView view;

    public SaveController(MemberManager model, MemberManagerView view) {
        this.model = model;
        this.view = view;
    }

    @Override
    public void handle(ActionEvent event) {
        model.saveToFile();
        System.out.println("saved");
    }
}
