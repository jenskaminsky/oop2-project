package ch.fhnw.view;

import ch.fhnw.model.Member;
import ch.fhnw.model.MemberManager;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class SaveMemberChangesController implements EventHandler<ActionEvent> {

    MemberManager model;
    DetailView view;
    Member before;
    MemberManagerView context;

    public SaveMemberChangesController(MemberManager model, DetailView view, Member before, MemberManagerView context) {
        this.model = model;
        this.view = view;
        this.before = before;
        this.context = context;
    }

    @Override
    public void handle(ActionEvent event) {
        view.validateFields();
        if (view.areFieldsValid()) {
            Member after = view.getMemberFromFields();
            model.update(before, after);
            context.updateUndoRedoBtn();
            context.memberTable.getSelectionModel().select(after);
        }
    }
}
