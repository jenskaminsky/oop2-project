package ch.fhnw.view;

import ch.fhnw.model.Member;
import ch.fhnw.model.MemberManager;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class TopTreeView {

    private final static String STAGE_ICON_PATH = "file:src/main/resources/icon.png";
    private final static String CSS_PATH = "file:src/main/resources/MemberManagerView.css";
    private Stage stage = new Stage();
    private GridPane mainPane = new GridPane();
    private Scene scene = new Scene(mainPane);

    private MemberManager model;

    public TopTreeView(MemberManager model, String title) {
        this.model = model;

        stage.setTitle(title);
        stage.setResizable(false);
        stage.getIcons().add(new Image(STAGE_ICON_PATH));
        
        mainPane.setHgap(20);
        mainPane.setVgap(10);
        mainPane.setPadding(new Insets(10, 10, 10, 10));

        mainPane.getColumnConstraints().add(0, new ColumnConstraints(150));
        mainPane.getColumnConstraints().add(1, new ColumnConstraints(250));

        List<Member> topThreeMemberSorted = model.read().stream()
                .sorted(new eloComparator())
                .collect(Collectors.toList());

        int rankIdx = 0;
        while (rankIdx < 3 && rankIdx < topThreeMemberSorted.size()) {
            Member member = topThreeMemberSorted.get(rankIdx);
            VBox details = new VBox();
            Label eloScoreLbl = new Label(String.valueOf(member.eloScore));
            Label firstNameLbl = new Label(member.firstName);
            Label lastNameLbl = new Label(member.lastName);
            details.getChildren().addAll(eloScoreLbl, firstNameLbl, lastNameLbl);

            ImageView avatar = new ImageView();
            avatar.setFitHeight(150);
            avatar.setFitWidth(150);
            DetailView.tryUpdateAvatar(member.avatarPath.getPath(), avatar);

            mainPane.add(details, 1, rankIdx);
            mainPane.add(avatar, 0, rankIdx);

            rankIdx++;
        }
        scene.getStylesheets().add(CSS_PATH);
        stage.setScene(scene);
    }

    void start() {
        stage.show();
    }

    private class eloComparator implements Comparator<Member> {

        @Override
        public int compare(Member o1, Member o2) {
            return o2.eloScore - o1.eloScore;
        }
    }
}
